NAME    := emailhippo/nuget
IMG     := ${NAME}:${BITBUCKET_BUILD_NUMBER}
LATEST  := ${NAME}:latest

build:
	@docker build -t ${IMG} .
	@docker tag ${IMG} ${LATEST}

push:
	@docker push ${NAME}

login:
	@docker login -u ${DOCKER_USER} -p ${DOCKER_PASS}
