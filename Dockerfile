FROM debian:stable-20200607

# MAINTAINER Email Hippo (https://www.emailhippo.com)

# Install any needed packages
RUN apt update \
	&& apt install apt-transport-https dirmngr gnupg ca-certificates -y \
	&& apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF \
	&& echo "deb https://download.mono-project.com/repo/debian stable-buster main" | tee /etc/apt/sources.list.d/mono-official-stable.list \
	&& apt update \
	&& apt install mono-devel -y \
	&& apt install -y apt-transport-https ftp git wget gpg curl \
	&& wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.asc.gpg \
	&& mv microsoft.asc.gpg /etc/apt/trusted.gpg.d/ \
	&& wget -q https://packages.microsoft.com/config/debian/10/prod.list \
	&& mv prod.list /etc/apt/sources.list.d/microsoft-prod.list \
	&& chown root:root /etc/apt/trusted.gpg.d/microsoft.asc.gpg \
	&& chown root:root /etc/apt/sources.list.d/microsoft-prod.list \
	&& apt update \
	&& apt install dotnet-sdk-3.1 -y \
	&& curl -o /usr/local/bin/nuget.exe https://dist.nuget.org/win-x86-commandline/latest/nuget.exe \
	&& sleep 4 \
	&& echo "alias nuget='mono /usr/local/bin/nuget.exe'" | tee -a /root/.bashrc
